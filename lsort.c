/*
lsort - re-sort groupinfo files from versions prior to 1.9.3

Written and Copyright 1999 by Joerg Dietrich <joerg@dietrich.net>
Modified and copyright of the modifications 2002 by Ralf Wildenhues
<ralf.wildenhues@gmx.de>.
Modified and copyright of the modifications 2002, 2009 by Matthias
Andree <matthias.andree@gmx.de>

See file COPYING for restrictions on the use of this software.
*/

#include "leafnode.h"
#include "critmem.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

int verbose = 0;
int debug = 0;

static int
comp(const void *a, const void *b)
{
    return strcasecmp(*(const char *const *)a, *(const char *const *)b);
}

int
main(void)
{
    char *l;
    char *path; /* RATS: ignore */
    size_t l_path;
    const char *tackon = "/leaf.node/groupinfo.old";
    const char *const myname = "lsort";
    char **act = NULL;
    unsigned long acount = 0, allocd = 0;
    unsigned long i;
    FILE *f;
    int err = 0;

    critsyslog(0);
    l_path = strlen(spooldir) + strlen(tackon) + 1;
    path = critmalloc(l_path + 1, myname);
    xsnprintf(path, l_path, "%s%s", spooldir, tackon);

    f = fopen(path, "r");
    if (f == NULL) {
	fprintf(stderr, "lsort: cannot open \"%s\": %s.\n", path, strerror(errno));
	exit(EXIT_FAILURE);
    }

    while ((l = getaline(f))) {
	if (acount >= allocd) {
	    allocd ? (allocd <<= 1) : (allocd = 256);
	    act = (char **)critrealloc((char *)act, allocd * sizeof(char *), myname);
	}
	act[acount] = critstrdup(l, myname);
	acount++;
    }
    fclose(f);

    qsort(act, acount, sizeof(char *), &comp);

    clearerr(stdout);
    for (i = 0; i < acount; i++) {
	int ok;
	ok = printf("%s\n", act[i]);
	free(act[i]);
	if (ok < 0) { err = 1; break; }
    }
    free(act);

    if (fflush(stdout)) err = 1;
    if (ferror(stdout)) err = 1;
    if (err) { perror("lsort"); }
    return err ? EXIT_FAILURE : EXIT_SUCCESS;
}
