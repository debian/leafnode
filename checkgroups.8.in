.TH checkgroups 8 "@VERSION@" "leafnode" \" -*- nroff -*-
.\" DO NOT EDIT: @configure_input@
.\"
.\" Written by Arnt Gulbrandsen <agulbra@troll.no> and copyright 1995-96 Troll
.\" Tech AS, Postboks 6133 Etterstad, 0602 Oslo, Norway, fax +47 22646949.
.\"
.\" Use, modification and distribution is allowed without limitation,
.\" warranty, or liability of any kind.
.\"
.\" Modified and (C) Copyright of modifications 2001-2021 by Matthias Andree
.\"
.\" hilit19 is stupid: "
.SH NAME
checkgroups \- update group descriptions

.SH SYNOPSIS
.B checkgroups
.I checkgroupsfile

.SH DESCRIPTION
.B Leafnode
is a USENET package intended for small sites, where there are few
users and little disk space, but where a large number of groups is
desired.
.PP
.B Checkgroups
is the program which parses checkgroup scripts to include new/updated
group descriptions into the active file (which is usually
\fB@SPOOLDIR@/leaf.node/groupinfo\fR).
.PP
.B Checkgroups
sets its real and effective uid to "news".

.SH OPTIONS
.TP
.I checkgroupsfile
is a file which contains the newsgroup name and the description on one
line, separated by whitespace. Checkgroups scripts are sometimes sent
out by news administrators (e.g., in the bionet.* hierarchy).

.SH ENVIRONMENT
.TP
.B
LN_LOCK_TIMEOUT
This variable is parsed as an unsigned integer value and determines how
many seconds checkgroups will wait when trying to obtain the lock file
from another leafnode program. 0 means to wait indefinitely. This
variable takes precedence over the configuration file.

.SH AUTHOR
Written by Cornelius Krasel <krasel@wpxx02.toxi.uni\-wuerzburg.de>.
Copyright 1997\~\[en]\~1999.
.PP
Modified by Matthias Andree <matthias.andree@gmx.de>.
Copyright 2002\~\[en]\~2004.
.PP
Leafnode was originally written by Arnt Gulbrandsen <agulbra@troll.no>
and is copyright 1995\~\[en]\~1996
Troll Tech AS, Postboks 6133 Etterstad, 0602 Oslo, Norway, fax +47
22646949.

.SH SEE ALSO
.BR leafnode (8),
.BR texpire (8),
.BR "RFC 977".
