#include <stdio.h>
#include "system.h"
#include "nntpd.h"

void
dodate(void)
{
    time_t t;
    if (time(&t) == (time_t)-1) {
	printf("500 Internal error\r\n");
    } else {
	struct tm *tm;
	tm = gmtime(&t);
	printf("111 %04d%02d%02d%02d%02d%02d\r\n",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec);
    }
}


