Preparations:

1.  Make sure you have the PCRE library and - if they are split out in your
    distribution - developer packages installed. The names of these packages
    vary, some call the package pcre, some libpcre, some libpcre0, and
    sometimes the developer stuff (static library, headers, library
    documentation) is part of the main package and sometimes it is split out
    into a package named similarly with a -dev or -devel suffix.

    If your distribution does not ship PCRE, get it from http://www.pcre.org/
    and install it before trying to install leafnode.

2.  If you have a version of leafnode installed from your distributor's
    packages, and plan to install from source, it may be a good idea to
    uninstall the old package first, to avoid later conflicts with the
    packaging system. "dpkg -r leafnode" or "rpm -e leafnode" are typical
    commands.

    If you plan instead to overwrite the distributor's package in place, you
    can simply drop the package information from the package data base, for
    instance, "rpm -e --justdb leafnode". You need to make sure to use the same
    path configuration options in installation step #1 below that your packager
    had originally used.

3.  If you've installed a previous version from source, make sure to use the
    same paths for ./configure in installation step #1 below. Write down the
    configuration path if you've given extra arguments.

Installation:

The steps are marked "as user" or "as root". All steps marked "as user" should
be run by an unprivileged user, NOT ROOT, to prevent inadvertent damage to the
system. The steps marked "as root" require root privileges and can only be
executed either by the super user or by means of a corresponding wrapper such
as sudo.

0.  (as user) On Solaris, make sure that /usr/xpg4/bin and /usr/ccs/bin are in
    your PATH and *before* /usr/bin or /bin.

1.  (as user) Since version 1.9, leafnode uses GNU autoconf to determine what
    machine it will run on. Type

	sh ./configure

    to create an appropriate Makefile and config.h.  If you have and want to
    use IPv6 support, invoke "configure" with the --with-ipv6 flag.

    If ./configure has trouble finding certain programs, you can check
    ./configure --help for information on supported environment variables,
    example remedy when ./configure cannot find PCRE and your system has it
    installed in /opt:

    env PCRE2CONFIG=/opt/pcre2/bin/pcre2-config sh ./configure


    See the README file's "UPDATING FROM VERSIONS BEFORE 1.9.23" section for
    subtle differences of IPv4 and IPv6 support.

    Remember that when you are updating a previously installed version of
    leafnode, you need to configure the new version of leafnode to overwrite
    the old one, although changing the lockfile location is fine if you make
    sure no leafnode process is running when you install.

    You configure paths with the "configure --prefix" option. For example, if
    leafnode had been previously installed in /opt, you would configure
    leafnode with the command "./configure --prefix=/opt".  Further options are
    --with-spooldir, --sysconfdir, --with-lockfile to determine the location of
    the spooldir (defaults to /var/spool/news), configuration directory
    (defaults to /etc/leafnode if prefix is unset, PREFIX/etc otherwise) and of
    the lock file (defaults to leaf.node/lock.file below the spooldir).

 ## Since 1.9.52, you can also configure the user and group account that
 ## leafnode switches to when started as "news", by means of the --with-user
 ## and --with-group options. DO MAKE SURE that the --with-user is a member of
 ## the group you give to the --with-group option (on most systems, the "id" or
 ## "groups" command with a user account name should tell you what groups the
 ## user is in.)

    NOTE: DO MAKE SURE that the lock file is in a directory that is writable by
    the user "news". Leafnode will have dropped root privileges when it tries
    to obtain the lock file, and it needs write access. The new default (as of
    1.9.37) should be fine in most cases.

    Type make config.c and read the resulting config.c files to see which paths
    leafnode uses.

2.  (as user) Type

	make

    There should be no errors. On Solaris, you may get some "function
    declaration isn't a prototype" warnings. These are harmless. Expect other
    warnings on Solaris as well, from experience: the older your OS, the more.

    Then type

	make check

    All tests must pass, otherwise, ask on the leafnode mailing list for help.

3a. (as root) Create a "news" group if you don't have one.

3b. (as root) Create a "news" user if you don't have one. Its primary group
    should be "news", with no supplemental groups.

3c. (as root) Create an alias in your mail system to forward mail addressed to
    "news" to the real user who looks after leafnode.

    If you run qmail: get and install the fastforward package.
    It is available from qmail distribution sites.
    Consider using a better mail package than qmail and plan a migration.
    Common choices are Postfix, Exim and Courier.

    Find your aliases file (/etc/aliases or /etc/mail/aliases), add a line
    "news: joe" (assuming joe looks after your leafnode) and then type

	newaliases.

4.  (as root) Type

	make install

5a. (as root) If you are updating leafnode from a version before 1.9.3
    (including all 1.9.3 beta versions before 1.9.3b5), "make update". This
    will call the update.sh shell script provided with leafnode. Your groupinfo
    file will be reformatted and some other files will be moved around. In case
    something goes wrong, you will find your old groupinfo file in
    /var/spool/news/leaf.node/groupinfo.old. If everything works out correctly,
    you can delete this file (it is not needed any more).

5b. (as root) Read the NEWS file and the README file's updating sections
    completely. You may wish to re-read the sections on 1.9.20 and 1.9.23 even
    if you're running a newer version, to become aware of some possible causes
    for problems.

6.  (as root) Edit $(sysconfdir)/config ($(sysconfdir) defaults to
    /etc/leafnode if it is unset and prefix is unset, to $prefix/etc if prefix
    is set).  For documentation, see config.example and leafnode(8).

6a. It is absolutely necessary to change the "server" parameter to point it to
    your upstream news server (generally the one of your IP provider).

6b. Make sure $NNTPSERVER or /etc/nntpserver points to your own host so clients
    will talk to leafnode rather than try to go to the upstream server.  If you
    want to use filtering of the incoming spool, see "FILTER FILE" in the
    README.

6c. You definitely want to use "initialfetch = 200" or something in the config
    file, to prevent fetchnews from filling up your disk when hitting a
    high-volume newsgroup.

7.  (as root) If your system does not have a fully qualified domain name
    (Debian default installs belong to this group of systems), edit /etc/hosts
    to add one, for example, if your /etc/hosts has a line

    192.168.0.1 debian

    make that

    192.168.0.1 debian.example.com debian

    IMPORTANT: debian.example.com must be replaced by a real fully qualified
    domain name. Making up host names will cause trouble, like posts being
    discarded at the upstream server, posts being discarded when Message-IDs of
    different postings clash and other troubles. See one of the README-FQDN*
    files for details.

8.  (as news) Set up a cron job to run texpire every night or at least every
    week.  Here is my crontab line, which runs nightly:

    0 4 * * * /usr/local/sbin/texpire

    On some systems, you may need to allow the user "news" to install/edit
    crontabs, some of these use the file /etc/cron.allow to store this
    information. Check the information for your cron system, man cron or man
    crontab should provide you with details.

    I did "su news -c 'crontab -e'" as root to edit the crontab file, and added
    this line.  Substituting "1" for the third "*", thus:

    0 4 * * 1 /usr/local/sbin/texpire

    tells cron to run texpire at 4am Monday morning. See man crontab for
    details.

9.  Make sure fetchnews is run at the appropriate time.  If you have a
    full-time link, run it from cron (as "news" again), if not, run it when
    your connection to the net is established.  If it is run as root, it will
    change its user ID to "news" automatically. If you use PPP, you can
    probably run fetchnews from /etc/ppp/ip-up.local or /etc/ppp/ip-up.

10. (as root) Edit /etc/hosts.deny to add a line:
    leafnode: ALL

    Edit /etc/hosts.allow to add a line:
    leafnode: 127.0.0.1

    If you run leafnode in a LAN, you can of course add netmasks for the hosts
    in your LAN. Assuming your LAN used 192.168.0.*, the /etc/hosts.allow line
    might look like this:

    leafnode: 127.0.0.1 192.168.0.0/255.255.255.0

    See man hosts_access(5) and hosts_options(5) for more information.

11. How leafnode is actually started, depends on what facilities systems
    provide for. USE EXACTLY (ONLY) ONE OF THESE SUGGESTIONS IN THE SUBSECTIONS
    OF THIS SECTION.

    A WORD OF WARNING: regardless of how you do access-control, do not use
    name-based access control unless you know what you're doing. Use IPs (like
    1.2.3.4) instead. Do not use names that point to dynamic IP addresses or
    use dynamic IP addresses for access control!

    The options to start the NNTP server are (remember, pick only one):

    a) Traditionally, the software to start network daemons on demand was
       inetd, and it still is on e. g. HP-UX 10, Solaris 8, *BSD and others --
       see section 11a below. However, most inetd implementations have design
       flaws, so this way is only recommended on FreeBSD for now.  The
       configuration file for inetd is usually /etc/inetd.conf or
       /etc/inet/inetd.conf.

    b) Then, xinetd has joined in and is now the default on Red Hat and
       SUSE Linux, it is described in section 11b.

11a.ONLY WHEN USING INETD (Beware, many systems ship xinetd nowadays, see
    section 11b below instead.)

    (as root) Edit /etc/inetd.conf so that $(BINDIR)/leafnode is executed for
    incoming NNTP connections.  Here is my inetd.conf line (insert it at the
    leftmost column, without leading spaces!):

    nntp stream tcp nowait news /usr/sbin/tcpd /usr/local/sbin/leafnode

    This starts leafnode for all connections on the nntp port, subject to
    /etc/hosts.allow and /etc/hosts.deny screening.  If you don't have
    /usr/sbin/tcpd, fetch the tcp_wrappers package and install it.

    Using leafnode without tcpd is not supported and opens your computer to
    abuse (even happens on modem lines that are only connected during the
    fetch!)

    After these changes, force inetd to read the changed configuration file by
    sending it the HANGUP signal. To achieve this, issue the following command
    (as root):

    kill -HUP `cat /var/run/inetd.pid`

    Proceed to step #12.

11b.ONLY WHEN USING XINETD
    (as root)

    xinetd versions before 2.3.10 are not supported. It may or may not work for
    you with older versions.

    More documentation is available in the xinetd and xinetd.conf manual pages.
    Try man xinetd.

     a. check if your /etc/xinetd.conf has a "includedir /etc/xinetd.d" line.
	If it has, store the configuration snippet below as
	/etc/xinetd.d/leafnode. If your xinetd.conf does NOT have that
	includedir line, append the configuration snippet to your
	/etc/xinetd.conf.

     b. After you have figured which of the two files to edit (or create), add
	this to the xinetd configuration file:

	service nntp
	{
	    flags           = NAMEINARGS NOLIBWRAP
	    socket_type     = stream
	    protocol        = tcp
	    wait            = no
	    user            = news
	    server          = /usr/sbin/tcpd
	    server_args     = /usr/local/sbin/leafnode
	    instances       = 7
	    per_source      = 3
	}

    This allows at most 7 leafnode connections. At most 3 connections are
    allowed from the same client host at the same time. Adjust these figures if
    necessary.

    Send xinetd a USR2 or HUP signal (check the documentation which one you
    need, it depends on the xinetd version) to make it re-read its
    configuration.  Figure its PID with ps ax | egrep '[x]inetd' on Linux or
    *BSD or ps -ef | egrep '[x]inetd' on SysV machines (Solaris), then do:
    kill -s USR2 12345, replacing 12345 with the PID you just found out.

    Proceed to step #12.

12. (as root or news) Run fetchnews.  The first run will take some time since
    fetchnews reads a list of all newsgroups from your upstream server.  With
    an 28.8 modem, it can take as long as 60 minutes (depending on how many
    newsgroups your provider offers). To see fetchnews working, run it with
    -vvv. If you update leafnode from a version prior to 1.6, the format of
    the groupinfo file has changed. You should therefore run fetchnews with the
    parameter -f.

13. (as user) Read news using an NNTP client (newsreader).

    Text-based clients often look into the environment variable NNTPSERVER or
    the file /etc/nntpserver to find where to connect, but will usually also
    accept a host name on the command line. You'd put the name of your leafnode
    server into /etc/nntpserver or the NNTPSERVER variable. Usually, using the
    IP in dotted-quad notation (such as 10.11.12.13) will also work.

    Newsreaders with graphical user interface usually have some sort of
    configuration dialogue or wizard that lets you enter the server's name or
    IP in dotted-quad notation and port. (The port is usually 119.)

    Note that leafnode doesn't currently support SSL or TLS, "encryption". If
    you need SSL support, check out the third-party stunnel package.
    Configuring leafnode with stunnel is beyond the scope of this document.

    After this, you should have empty files in
    /var/spool/news/interesting.groups/ for every group you want to read.

14. (as root or news) Run fetchnews again. This run should pick up all the
    groups you want to read.

15. Note: for access from remote sites, outside of your LAN, additional
    leafnode configuration is required since leafnode 1.9.23. See README and
    config.example for details.

Cornelius Krasel <krasel@wpxx02.toxi.uni-wuerzburg.de>
Matthias Andree <matthias.andree@gmx.de>

 vim:ai fo=tn2 tw=79
