/*
 * critmem.c -- memory allocation that only returns on success
 *
 * (C) 2000 - 2002 by Matthias Andree <matthias.andree@gmx.de>
 * Backport of critstrdup courtesy of Ralf Wildenhues <ralf.wildenhues@gmx.de>
 *
 * ---------------------------------------------------------------------------
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 or 2.1 of
 * the License. See the file COPYING.LGPL for details.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif

#include "critmem.h"

#include <stdlib.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>

static char ebuf[1024]; /* error buffer */

static int log_syslog = 1; /* log to syslog? */

static void barf(const char *func, size_t size, const char *message) {
    snprintf(ebuf, sizeof(ebuf), "%s(%ld) failed: %s", func, (long)size, message);
    if (log_syslog)
	syslog(LOG_ERR, "%s", ebuf);
    fwrite(ebuf, strlen(ebuf), 1, stderr);
    fwrite("\n", 1, 1, stderr);
    exit(EXIT_FAILURE);
}

/*
 * replacement for malloc, syslogs allocation failures
 * and exits with the error message
 */
char *
critmalloc(size_t size, const char *message)
{
    char *a;

    a = (char *)malloc(size);
    /* we don't want to return NULL, not even for size == 0 */
    if (!a && size == 0)
	a = (char *)malloc(1);
    if (!a)
	barf("malloc", size, message);
    return a;
}

/*
 * replacement for realloc, syslogs allocation failures
 * and exits with the error message
 */
char *
critrealloc(char *a, size_t size, const char *message)
{
    char *b = (char *)realloc(a, size);
    if (!b && size != 0)
	barf("realloc", size, message);
    return b;
}

/*
 * replacement for malloc, logs allocation failures
 * and exits with the error message
 */
char *
critstrdup(const char *source, const char *message)
{
    char *a;

    a = (char *)critmalloc(strlen(source) + 1, message);
    strcpy(a, source);		/* RATS: ignore */
    return a;
}

void critsyslog(int do_log) {
    log_syslog = do_log;
}
