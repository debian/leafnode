/* testgen.c -- test generator
 * (C) 2000 - 2002 by Matthias Andree <matthias.andree@gmx.de>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 or 2.1 of
 * the License. See the file COPYING.LGPL for details.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "leafnode.h"
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int verbose = 5;
int debug = 1;

int
main(int argc, char **argv)
{
    unsigned long len, i;
    /* arguments: size byte */
    if (argc < 3) {
	exit(1);
    }
    len = strtoul(argv[1], 0, 10);
    for (i = 0; i < len; i++) {
	putc(atoi(argv[2 + i % (argc - 2)]), stdout);
    }
    return 0;
}
