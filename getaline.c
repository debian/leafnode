/*
    getaline - fetch a single line from a stdio stream, arbitrary length
    Copyright (C) 2000 - 2010 Matthias Andree

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    As special permission, when linked against the leafnode package,
    leafnode programs themselves need not be put under the GPL.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "leafnode.h"

#include <stdio.h>
#include <sys/types.h>
#include <syslog.h>
#include <string.h>
#include <sys/stat.h>
#include "ln_log.h"

static char *
ygetaline(int mode /** 0: regular, 1: free memory */, FILE * f)
{
    static char *buf;		/* buffer for line */
    static size_t size;		/* size of buffer */
    ssize_t len;		/* # of chars stored into buf before '\0' */
    int have_logged = 0;	/* variable to track if current input line was already logged */

    if (mode) {
	free(buf);
	return NULL;
    }

    len = getline(&buf, &size, f);
    if (len < 0) {
	if (ferror(f)) {
	    if (debug)
		syslog(LOG_DEBUG, "< (ERROR: %s)", strerror(errno));
	} else {
	    if (debug > 1)
		syslog(LOG_DEBUG, "< (EOF)");
	}
	return 0;
    }
    if (len && (buf[len - 1] == '\n')) {	/* go back on top of the newline */
	--len;
	while (len && (buf[len - 1] == '\r'))	/* also delete CR */
	    --len;
    } else {
	struct stat st;
	int err = 0;
	/* allow missing LF only on regular files */

	if (fstat(fileno(f),&st) || !S_ISREG(st.st_mode)) err = 1;

	if (err || debug)
	    ln_log(err ? LNLOG_SERR : LNLOG_SDEBUG, LNLOG_CTOP,
		    "<%s (%s: no trailing LF)", buf, err ? "error" : "debug");
	have_logged = 1;

	if (err)
	    return NULL;
    }

    buf[len] = '\0';		/* unconditionally terminate string,
				   possibly overwriting newline */

    if (debug > 1 && !have_logged)
	syslog(LOG_DEBUG, "<%s", buf);
    return buf;
}

char *getaline(FILE *f)
{
    return ygetaline(0, f);
}

void freegetaline(void)
{
    (void)ygetaline(1, NULL);
}
