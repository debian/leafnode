# DO NOT EDIT!
# leafnode.spec.  Generated from leafnode.spec.in by configure.

Version: 1.12.0
Summary: Leafnode - a leafsite NNTP server (Version %version)
Summary(de): Leafnode - ein offline-Newsserver (Version %version)
Name: leafnode
Release: 1
License: GPL
Group: Applications/News
Source: http://prdownloads.sourceforge.net/leafnode/leafnode-%version.tar.xz
Prefix: /usr
# Distribution: ${DISTRIBUTION}
Buildroot: /var/tmp/leafnode-%version-buildroot/
Packager: Matthias Andree <matthias.andree@gmx.de>
BuildPrereq: libpcre2-8.so.0
Requires: xinetd
Conflicts: inn cnews

# WARNING: DO -NOT- REMOVE THE /leafnode COMPONENT FROM confdir!
# If you do, the RPM will subvert original system permissions.
%define confdir /etc/leafnode
%define spooldir /var/spool/news
%define lockfile /var/spool/news/leaf.node/lock.file

%description
Leafnode is a small NNTP server for leaf sites without permanent
connection to the internet. It supports a subset of NNTP and is able to
automatically fetch the newsgroups the user reads regularly from the
newsserver of the ISP.

%description -l de
Leafnode ist ein offline-Newsserver, der vor allem für den typischen
Einzelnutzer-Rechner ohne permanente Internetanbindung geeignet ist.
Leafnode bezieht automatisch die Newsgroups, die der oder die Nutzer
regelmäßig lesen, vom Newsserver des Providers.

%prep
test -f /usr/include/pcre/pcre2.h \
|| test -f /usr/include/pcre2.h \
|| { echo >&2 "You MUST install the pcre2-devel package before building this RPM." ; exit 1 ; }
%setup
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix --mandir=%_mandir \
 --with-spooldir=%spooldir\
 --sysconfdir=%confdir --with-lockfile=%lockfile

%build
make

%install
rm -rf $RPM_BUILD_ROOT

make DESTDIR=$RPM_BUILD_ROOT install
for i in $RPM_BUILD_ROOT/%{confdir}/*.example ; do
  cp -p $i ${i%%.example}
done

# texpire cron job
mkdir -p $RPM_BUILD_ROOT/etc/cron.daily
install -m755 leafnode.cron.daily \
    $RPM_BUILD_ROOT/etc/cron.daily/leafnode

# xinetd sample file
mkdir -p $RPM_BUILD_ROOT/etc/xinetd.d
install -m644 leafnode.xinetd $RPM_BUILD_ROOT/etc/xinetd.d/leafnode

# German manual pages and documentation
for i in 1 8 ; do
  mkdir -p $RPM_BUILD_ROOT/%{_mandir}/de/man$i
  install -m644 doc_german/*.$i $RPM_BUILD_ROOT/%{_mandir}/de/man$i
  #gzip -9f $RPM_BUILD_ROOT/%{_mandir}/de/man/man$i/*.?
done
rm -f doc_german/*.in doc_german/*.? doc_german/Makefile*

cat >README.rpm <<EOF
Note: this RPM has pre-installed a configuration file for xinetd.
Read /etc/xinetd.d/leafnode for the prerequisites to use it.
EOF

%files
%defattr(-,root,root)
%attr(750,root,news) %dir %confdir/

%config(noreplace) %attr(640,root,news) %confdir/config
%config(noreplace) %attr(640,root,news) %confdir/filters
%config(noreplace) /etc/cron.daily/leafnode
%config(noreplace) /etc/xinetd.d/leafnode

%attr(640,root,news) %confdir/config.example
%attr(640,root,news) %confdir/filters.example
%prefix/bin/*
%prefix/sbin/*

%doc %_mandir/man1/*
%doc %_mandir/man8/*
%doc %_mandir/de/*/*

%doc COPYING COPYING.GPL COPYING.LGPL CREDITS ChangeLog ChangeLog.old
%doc FAQ.txt FAQ.html FAQ.pdf INSTALL
%doc NEWS OLDNEWS README* ADD-ONS
%doc doc_german
%doc leafnode-SA-*
%doc UNINSTALL-daemontools

%doc %attr(755,root,root) tools/*.pl

%dir %attr(2755,news,news) %spooldir
%dir %attr(2755,news,news) %spooldir/leaf.node
%dir %attr(775,news,news) %spooldir/failed.postings
%dir %attr(775,news,news) %spooldir/interesting.groups
%dir %attr(2755,news,news) %spooldir/out.going
%dir %attr(755,news,news) %spooldir/temp.files
%dir %attr(2755,news,news) %spooldir/message.id

%post
echo "Remember to edit /etc/xinetd.d/leafnode and reload xinetd."

%clean
rm -rf $RPM_BUILD_ROOT %{_builddir}/%name-%version
