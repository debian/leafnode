# Leafnode debconf templates basque translation
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Piarres Beobide <pi@beobide.net>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Leafnode debconf templates\n"
"Report-Msgid-Bugs-To: leafnode@packages.debian.org\n"
"POT-Creation-Date: 2008-02-18 08:13+0100\n"
"PO-Revision-Date: 2008-02-18 10:45+0100\n"
"Last-Translator: Piarres Beobide <pi@beobide.net>\n"
"Language-Team: Euskara <Librezale@librezale.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:2001
msgid "Server to download news from:"
msgstr "Bertatik berri-taldeak deskargatzeko zerbitzaria:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"The name of the news server should be provided by the organization providing "
"you with network access, such as your Internet Service Provider."
msgstr ""
"Berri-tade zerbitzariaren izena zuri internet-eko zerbitzua salzen dizun "
"horniztaileak eman beharko lizuke."

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"This server is generally called \"news.<domain>\" or \"nntp.<domain>\" where "
"<domain> is the local domain name."
msgstr ""
"Zerbitzari honen izen arruntena \"news.>domeinua>\" edo \"nntp.<domeinua>\" "
"izaten da <domeinua> dominu lokala delarik."

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Enable access controls for Leafnode?"
msgstr "Leafnode-ren sarrera kontrolak gaitu?"

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"If you do not enable some access controls for Leafnode, people everywhere "
"will be able to use the news server which opens opportunities for spamming "
"or resource abuse."
msgstr ""
"Ez baduzu sarrera kontrol batenbat gaitzen, edozein berri-taldea erabili "
"ahal izango du, honek zabor-posta bidaltzaileei errekurtsoez baliatzeko "
"aukera ematen."

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"Access controls will prevent computers other than the news server itself "
"reading or posting to newsgroups using the server. If required access can be "
"granted to other computers by editing /etc/hosts.allow."
msgstr ""
"Sarrera kontrolak zerbitzaria bera ez den beste edozein ordenagailuk berri-"
"taldeak irakurri edo bidaltzeko aukera ezgaitzen du. Behar izanez gero beste "
"ordenagailuen sarrera /etc/hosts.allow fitxategiaren bidez onartu daiteke."

#. Type: select
#. Choices
#: ../templates:4001
msgid "permanent"
msgstr "finkoa"

#. Type: select
#. Choices
#: ../templates:4001
msgid "none"
msgstr "bate"

#. Type: select
#. Description
#: ../templates:4002
msgid "Network connection type:"
msgstr "Sare konexio mota:"

#. Type: select
#. Description
#: ../templates:4002
msgid "The Leafnode package can automatically download news."
msgstr "Leafnode paketeak berri-taldeak automatikoki deskarga ditzake."

#. Type: select
#. Description
#: ../templates:4002
msgid ""
"The method used for this depends on the network connection type. Scripts "
"provided with the package support two network connection types:\n"
" - permanent: hourly news downloads;\n"
" - PPP      : news downloads triggered by dialouts."
msgstr ""
"Honetarako erabilitako metodoa sare konexioaren arabera aldatzen da. "
"Paketeko Script-ek bi sare konexio mota onartzen dituzte:\n"
" - finkoa : berri-taldeak ordura deskargatzen dira;\n"
" - PPP    : Berri-taldeak koenxio markaketaren arabera egingo dira."

#. Type: select
#. Description
#: ../templates:4002
msgid "Either option will work for a dial-on-demand network connection."
msgstr "Edozein aukerak funtzionatuko du eskaerapeko markatzearekin. "

#. Type: select
#. Description
#: ../templates:4002
msgid ""
"Choosing 'none' will disable automatic news downloads. News can be "
"downloaded manually by running 'fetchnews'."
msgstr ""
"'batez' aukeratuaz berri-talde deskarga automatikoa ezgituko da. Berri-"
"taldeak ekuz deskargatu daitezke 'fetchnews' exekutatuaz."

#. Type: boolean
#. Description
#: ../templates:5001
msgid "Update the list of available groups?"
msgstr "Talde erabilgarrien zerrenda eguneratu?"

#. Type: boolean
#. Description
#: ../templates:5001
msgid ""
"Leafnode updates the list of available newsgroups when it checks for new "
"news. No newsgroups will be available until this has happened at least once."
msgstr ""
"Leafnode-k berr-talde erabilgarrien zerrenda eguneratzen du mezu berrien "
"bila arakatzean. HAu behintzat behin getatu arte ez da berri-talde "
"erabilgarririk egongo."

#. Type: boolean
#. Description
#: ../templates:5001
msgid ""
"If you choose to update the list of groups immediately, newsgroups will be "
"available to clients as soon as Leafnode has been set up."
msgstr ""
"Berri-taldeen zerrenda orain eguneratzea hautatzen baduzu, berri-taldeak "
"erabilgarri egongo dira erabiltzaileentzat leafnode abiarazten den bezain "
"laster."

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Remove news groups and articles when purging the package?"
msgstr "Berri-talde eta artikuluak ezabatu paketea garbitzean?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"The /var/spool/news directory holds the database of news articles downloaded "
"by Leafnode. Many other news servers also use this directory to store their "
"news database and you may wish to keep it even when removing the leafnode "
"package."
msgstr ""
"Leafnode-k deskargatzen dituen artikulu berrien datubasea /var/spool/news "
"direktorioan mantentzen da. BEste edozein berri-telade zerbitzarik ere "
"direktorio hori erabiltzen duenez leafnode paketea garbitzean direktorioa "
"mantendu nahi izan dezakezu."
